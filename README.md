# Assignment_4

Homework 4 Travel Agency Guidelines

The objective of this assignment is to practice how to populate a complex model with data. 
Consider the travel agency model we discussed in class. You are required to define the java classes associated with all the elements in the model. That includes the customer side, the physical airplanes, as well as the scheduling of flights. 

1. Create the travel agency object, 
    1. customer directory 
    1. master schedule
    1. Airliners
1. For each airliner, define a fleet of airplanes
1. For each airplane define its attributes and that include the number of seats (Seats and Seat Assignments will be accounted for on Flight for this homework assignment.) 
1. For each flight create associated seats. Seats come into 2 columns and each row is made up of six seats (window, middle, and Aisle). Assume each airplane will have 25 rows. 
1. For the customer directory create customers where some are assigned seats on flights. 

**NOTE: Card layout Should be extensively used** 

1. User should have an option to navigate to a Panel which allows multi-field/multi-parameter search of flights. Results should be visible on a JTable.
1. User should be able to reserve a seat for a Person/Customer. 
1. Use Dropdown wherever possible as manual text entries can be error some. Eg: while reserving a seat, user should only select from the seats that are available.
1. You need to do Exception Handling
1. Data Validation should be in place. Eg: Date should be in date format!
1. Please make multiple commits (u should have at least 3-5 commits) and use your own branch (Don’t direct commit on Master branch), failure to do so would have ramifications on your score. 
